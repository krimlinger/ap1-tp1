#ifndef ALEA_H
#define ALEA_H

int aleaEntier(int, int);
double aleaReel(double, double);

#endif
