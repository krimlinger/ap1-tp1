#include <stdlib.h>
#include <time.h>

int aleaEntier(int l, int h) {
    srand(time(NULL));
    return (rand() % (h-l+1)) + l;
}

double aleaReel(double l, double h) {
    srand(time(NULL));
    return (((double)rand()/ RAND_MAX) * (h-l)) + l;
}
