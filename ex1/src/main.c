#include <stdio.h>
#include <stdlib.h>
#include "../include/alea.h"

int main(void) {
    printf("nombre entier aleatoire: %d\n", aleaEntier(0, 4));
    printf("nombre reel aleatoire: %g\n", aleaReel(0, 4));
    return EXIT_SUCCESS;
}
