#include <stdio.h>
#include <stdlib.h>

int main(void) {
    unsigned int cout, p20, p10, p5, p1, i20, i10, i5, i1, r20, r10, r5, r1;
    printf("cout friandise: ");
    scanf("%u", &cout);
    printf("pieces de 20 presentes: ");
    scanf("%u", &p20);
    printf("pieces de 10 presentes: ");
    scanf("%u", &p10);
    printf("pieces de 5 presentes: ");
    scanf("%u", &p5);
    printf("pieces de 1 presentes: ");
    scanf("%u", &p1);

    printf("pieces de 20 introduites: ");
    scanf("%u", &i20);
    printf("pieces de 10 introduites: ");
    scanf("%u", &i10);
    printf("pieces de 5 introduites: ");
    scanf("%u", &i5);
    printf("pieces de 1 introduites: ");
    scanf("%u", &i1);   

    p20 += i20;
    p10 += i10;
    p5 += i5;
    p1 += i1;

    unsigned int paye = i20*20 + i10*10 + i5*5 + i1;
    
    if (cout > paye) {
        fprintf(stderr, "Pas assez de monnaie introduite\n");
        cout = paye;
    } else {
        cout =  paye - cout;
    }
    
    r20 = cout / 20;
    cout -= r20 * 20;
    r10 = cout / 10;
    cout -= r10 * 10;
    r5 = cout / 5;
    cout -= r5 * 5;
    r1 = cout;
    cout -= r1;
    
    printf("Resultat: on rend %u pieces de 20, %u pieces de 10, %u pieces de 5, %u pieces de 1\n", r20, r10, r5, r1);
    
    return EXIT_SUCCESS;
}
