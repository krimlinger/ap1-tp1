#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Taille par defaut du buffer utilise pour representer le nombre binaire.
#define TAILLE_BUFFER 8

// Tableaux passes par reference aux fonctions.
// On ne peut pas savoir leur taille avec sizeof(a)/sizeof(a[0]) donc on
// utilise une variable.
void inverser_tableau(int a[], size_t n) {
    for (size_t i = 0; i < (n / 2); i++) {
        int tmp = a[i];
        a[i] = a[n-i-1];
        a[n-i-1] = tmp;
    }
}

// Convertion d'un decimal d en binaire en utilisant la definition d'un nombre
// binaire sous forme polynomiale.
int num_b10tob2(int d) {
    int b = 0;
    for (int i = 0; d; i++, d /= 2) {
        b += (int)(pow(10, i)) * (d % 2);
    }
    return b;
}

// Convertion d'un decimal d en binaire en utilisant un tableau de taille n.
// Cette fonction peut retourner une erreur si le nombre binaire a plus de
// digits que TAILLE_BUFFER (on evite une erreur de segmentation).
void tableau_statique_b10tob2(int d, int b[], size_t n) {
    size_t i;
    for (i = 0; d; i++, d /= 2) {
        if (i >= n) {
            fprintf(stderr, "erreur: tableau_statique_b10tob2: nombre de "
                            "digit trop grand\n");
            exit(EXIT_FAILURE);
        }
        b[i] = d % 2;
    }
    inverser_tableau(b, i);
    // Utilise une -1 comme indicateur de fin du nombre binaire.
    b[i] = -1;
}

// Convertion d'un decimal d en binaire en utilisant une allocation memoire.
// Ne pas oublier de liberer le tableau alloue.
int* tableau_dynamique_b10tob2(int d) {
    size_t n = TAILLE_BUFFER;
    int *b = (int*)malloc(n*sizeof(int));
    if (!b) {
        goto echec_allocation;
    }
    size_t i;
    for (i = 0; d; i++, d /= 2) {
        // Buffer trop petit, on realloue
        if (i >= n) {
            n *= 2;
            b = (int*)realloc(b, n*sizeof(int));
            if (!b) {
                goto echec_allocation;
            }
        }
        b[i] = d % 2;
    }
    inverser_tableau(b, i);
    // Utilise une -1 comme indicateur de fin du nombre binaire.
    b[i] = -1;
    return b;

echec_allocation:
    free(b);
    perror("une allocation memoire a echoue");
    exit(EXIT_FAILURE);
}

int main(void) {
    int n=0;

    printf("Nombre decimal a convertir en binaire: ");
    scanf("%d", &n);

    // Premiere version.
    //printf("%d\n", num_b10tob2(n));

    // Seconde version.
    //int b[TAILLE_BUFFER] = { 0 };
    //tableau_statique_b10tob2(n, b, TAILLE_BUFFER); 
    //for (int i = 0; b[i] != -1; i++) {
    //    printf("%d", b[i]);
    //}

    // Troisieme version.
    int *c;
    c = tableau_dynamique_b10tob2(n);
    for (int i = 0; c[i] != -1; i++) {
        printf("%d", c[i]);
    }
    free(c);

    puts("");
    return EXIT_SUCCESS;
}
