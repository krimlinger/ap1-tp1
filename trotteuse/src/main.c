#include <stdio.h>
#include <stdlib.h>

int main(void) {
    unsigned int h, m, s;
    puts("T'as pas l'heure? ");
    scanf("%u %u %u",&h, &m, &s);
    s += 1;
    if (s >= 60) {
        s = 0;
        m += 1;
        if (m >= 60) {
            m = 0;
            h += 1;
            if (h >= 24) {
                h = 0;
            }
        }
    }
    printf("Dans une seconde, il sera %uh%um%us\n", h, m, s);
    return EXIT_SUCCESS;
}
