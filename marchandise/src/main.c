#include <stdio.h>
#include <stdlib.h>

enum transport { avion, bateau, train };
enum trajet { brest, paris, nice };

double prixColis(double, double, int);

double prixColis(double poids, double volume, int transport) {
    switch (transport) {
        case avion: return 6*poids;
        case bateau: return 5*volume;
        case train: return (4*poids) + (2*volume);
        default: fprintf(stderr, "transport inconnu\n"); exit(1);
    }
}

int main(void) {
    int trajet;
    double poids, volume;
    puts("Entrez le trajet(0,1,2) suivi du poids et volume:");
    scanf("%d %lf %lf", &trajet, &poids, &volume);
    switch (trajet) {
        case brest:
            puts((prixColis(poids, volume, avion) < prixColis(poids, volume, bateau)) ? "avion": "bateau");
            break;
        case paris:
            puts((prixColis(poids, volume, avion) < prixColis(poids, volume, train)) ? "avion": "train");
            break;
        case nice:
            puts((prixColis(poids, volume, train) < prixColis(poids, volume, bateau)) ? "train": "bateau");
            break;
        default: fprintf(stderr, "trajet inconnu\n"); exit(1);
    }
    
    return EXIT_SUCCESS;
}
